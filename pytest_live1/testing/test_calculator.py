# -*- coding:utf-8 -*-
'''
题目：

根据需求编写计算机器（加法和除法）相应的测试用例
在调用每个测试方法之前打印【开始计算】
在调用测试方法之后打印【结束计算】
调用完所有的测试用例最终输出【结束测试】
为用例添加标签
注意：

a、使用等价类，边界值，错误猜测等方法进行用例设计
b、用例中要添加断言，验证结果
c、灵活使用测试装置
'''

import pytest
from pytest_live1.pythoncode.calculator import Calculator


class TestCalculator:

    def setup_class(self):
        self.calc = Calculator()

    def setup(self):
        print("开始计算")

    def teardown(self):
        print("结束计算")

    def teardown_class(self):
        print("结束测试")

    @pytest.mark.P0
    @pytest.mark.parametrize('a,b,c',[[1,1,2],[-0.01,0.02,0.01],[10,0.02,10.02]],ids=['整型','浮点型','整型+浮点型'])
    def test_case1(self,a,b,c):
        result = self.calc.add(a,b)
        assert result == c

    @pytest.mark.P1
    @pytest.mark.parametrize('a,b,c',[[98.99,99,197.99],[99,98.99,197.99],[-98.99,-99,-197.99],[-99,-98.99,-197.99]],ids=['有效边界值-正1','有效边界值-正2','有效边界值-负1','有效边界值-负2'])
    def test_case2(self,a,b,c):
        result = self.calc.add(a,b)
        assert result == c

    @pytest.mark.P1
    @pytest.mark.parametrize('a,b,c',[[99.01,0,"参数大小超出范围"],[-99.01,-1,"参数大小超出范围"],[2,99.01,"参数大小超出范围"],[1,-99.01,"参数大小超出范围"]],ids=['边界值-超出范围1','边界值-超出范围2','边界值-超出范围3','边界值-超出范围4'])
    def test_case3(self,a,b,c):
        result = self.calc.add(a,b)
        assert result == c

    @pytest.mark.P1
    @pytest.mark.parametrize('a,b,errortype',[["文",9.3,TypeError],[4,"字",TypeError],["nu",0.2,TypeError],[30,"t",TypeError],["*&",0.2,TypeError],[21.45,"@",TypeError]],
                             ids=['异常-中文1','异常-中文2','异常-英文1','异常-英文2','异常-特殊字符1','异常-特殊字符2'])
    def test_case4(self,a,b,errortype):
        with pytest.raises(errortype) as e:
            result = self.calc.add(a,b)

    @pytest.mark.P2
    @pytest.mark.parametrize('a,b,errortype',[["",20.93,TypeError],[-3,"",TypeError],[" ",3.14,TypeError],[-90," ",TypeError]],ids=['异常-为空1','异常-为空2','异常-空格1','异常-空格2'])
    def test_case5(self,a,b,errortype):
        with pytest.raises(errortype) as e:
            result = self.calc.add(a,b)