# -*- coding:utf-8 -*-

'''
定义执行顺序，顺序为
先 add 方法 P1_1 和 P1_2 级别的用例
其次执行 P0 级别
最后执行 add 方法 P2
最终顺序为：add 用例 （P1_1>P1_2>P0）>  add 用例（P2）
生成测试报告
为测试类和方法添加分类
测试用例中添加日志，测试步骤，及图片
'''

import pytest
import yaml
import allure
from pytest_live1.pythoncode.calculator import Calculator

def get_datas(level):
    with open("../data/data.yaml") as f:
        result = yaml.safe_load(f)
        add_datas = result.get("add").get(level).get("datas")
        add_ids = result.get("add").get(level).get("ids")
    return [add_datas,add_ids]


@allure.feature("加法计算器用例")
class TestCalculator:

    add_P0_datas = get_datas("P0")[0]
    add_P0_ids = get_datas("P0")[1]
    add_P1_1_datas = get_datas("P1_1")[0]
    add_P1_1_ids = get_datas("P1_1")[1]
    add_P1_2_datas = get_datas("P1_2")[0]
    add_P1_2_ids = get_datas("P1_2")[1]
    add_P2_datas = get_datas("P2")[0]
    add_P2_ids = get_datas("P2")[1]

    def setup_class(self):
        self.calc = Calculator()

    def setup(self):
        print("开始计算")

    def teardown(self):
        print("结束计算")

    def teardown_class(self):
        print("结束测试")

    @allure.story("正向用例")
    @pytest.mark.run(order=3)
    @pytest.mark.P0
    @pytest.mark.parametrize('a,b,c',add_P0_datas,ids=add_P0_ids)
    def test_case1(self,a,b,c):
        result = self.calc.add(a,b)
        assert result == c

    @allure.story("边界值用例")
    @pytest.mark.run(order=1)
    @pytest.mark.P1_1
    @pytest.mark.parametrize('a,b,c',add_P1_1_datas,ids=add_P1_1_ids)
    def test_case2(self,a,b,c):
        result = self.calc.add(a,b)
        assert result == c

    @allure.story("特殊类型用例")
    @pytest.mark.run(order=2)
    @pytest.mark.P1_2
    @pytest.mark.parametrize('a,b,errortype',add_P1_2_datas,ids=add_P1_2_ids)
    def test_case3(self,a,b,errortype):
        with pytest.raises(eval(errortype)) as e:
            result = self.calc.add(a,b)

    @allure.story("异常用例")
    @pytest.mark.run(order=4)
    @pytest.mark.P2
    @pytest.mark.parametrize('a,b,errortype',add_P2_datas,ids=add_P2_ids)
    def test_case4(self,a,b,errortype):
        with pytest.raises(eval(errortype)) as e:
            result = self.calc.add(a,b)