# 导入 flask 和 flask_sqlalchemy

import yaml
from flask import Flask, request
from flask_restx import Api, Resource, Namespace
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy.orm import Session
from flask_jwt_extended import create_access_token
from flask_jwt_extended import current_user
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from back_live1.utils.log_utils import logger
from hmac import compare_digest
# 实例化app 对象
from sqlalchemy import *

app = Flask(__name__)
api =Api(app)

# 解决跨域问题
CORS(app,supports_credentials = True)

case_ns = Namespace("case",description="用例管理")
login_ns = Namespace("login",description="登录管理")

with open("../data/data.yaml") as f:
    result = yaml.safe_load(f)
    username = result.get("database").get('username')
    password = result.get("database").get('password')
    server = result.get("database").get('server')
    db = result.get("database").get('db')

app.config["JWT_SECRET_KEY"] = "super-secret"
app.config['SQLALCHEMY_DATABASE_URI'] = \
    f"mysql+pymysql://{username}:{password}@{server}/{db}?charset=utf8"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

jwt = JWTManager(app)
# SQLAlchemy 绑定 app
db = SQLAlchemy(app)
db_session:Session = db.session

# 用例表
class TestCase(db.Model):
    __tablename__ = "testcase"
    # 用例ID，用例的唯一标识
    id = Column(Integer, primary_key=True)
    # 用例的标题或者文件名
    case_title = Column(String(80), unique=True, nullable=False)
    # 备注
    remark = Column(String(120))

# 用户表
class UserDo(db.Model):
    # 定义表名
    __tablename__ = 'user'

    # 定义表字段
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    password = Column(String(255))

    def as_dict(self):
        # 用户对象转换为字典
        return {"id":self.id,"username":self.username,"password":self.password}

    # 检验密码
    def check_password(self, password):
        return compare_digest(password, self.password)

# 配置用户验证的条件
@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    logger.debug(f"_jwt_header {_jwt_header}")
    logger.debug(f"jwt_data {jwt_data}")
    # 拿到用户名
    identity = jwt_data["sub"]["username"]
    # 通过用户名到数据库中查询用户是否存在
    return UserDo.query.filter_by(username=identity).one_or_none()


@case_ns.route("")
class TestCaseServer(Resource):
    # token 鉴权
    decorators = [jwt_required()]
    get_parser = api.parser()
    get_parser.add_argument("id", type=int, location="args")

    @case_ns.expect(get_parser)
    def get(self):
        case_id = request.args.get("id")
        logger.info(f"接收到的参数：{case_id}")

        if case_id :
            case_data = TestCase.query.filter_by(id=case_id).first()
            logger.info({case_data.case_title})
            if case_data:
                datas = [{"id":case_data.id,
                          "case_title":case_data.case_title,
                          "remark":case_data.remark}]
            else:
                datas = []
        else:
            case_datas = TestCase.query.all()
            datas = [{"id":case_data.id,
                      "case_title":case_data.case_title,
                      "remark":case_data.remark}
                     for case_data in case_datas]
        return datas

    post_parser = api.parser()
    post_parser.add_argument("id",type=int,required=True,location="json")
    post_parser.add_argument("case_title",type=str,required=True,location="json")
    post_parser.add_argument("remark",type=str,location="json")

    @case_ns.expect(post_parser)
    def post(self):
        case_data = request.json
        logger.info(f"接收到的参数：{case_data}")
        case_id = case_data.get("id")
        exists = TestCase.query.filter_by(id=case_id).first()
        logger.info((f"查询结果：{exists}"))

        if not exists:
            testcase = TestCase(**case_data)
            logger.info(f"入库数据：{testcase}")
            db.session.add(testcase)
            db.session.commit()
            return {"code":0,"msg":f"case id {case_id} sucess add."}
        else:
            return {"code":4001,"msg":f"case id exists."}

    put_parser = api.parser()
    put_parser.add_argument("id",type=int,required=True,location="json")
    put_parser.add_argument("case_title", type=str, required=True, location="json")
    put_parser.add_argument("remark", type=str, location="json")

    @case_ns.expect(put_parser)
    def put(self):
        case_data = request.json
        case_id = case_data.get("id")
        logger.info(f"接收到的id：{case_id}")
        exists = TestCase.query.filter_by(id=case_id).first()

        if exists:
            case_data1 = {}
            case_data1["case_title"] = case_data.get("case_title")
            case_data1["remark"] = case_data.get("remark")
            TestCase.query.filter_by(id=case_id).update(case_data1)
            db.session.commit()
            return {"code":0,"msg":f"case id {case_id} sussess updata"}
        else:
            return {"code":40002,"msg":f"case id {case_id} not exists"}


    delete_parser = api.parser()
    delete_parser.add_argument("id",type=int,required=True,location="json")

    @case_ns.expect(delete_parser)
    def delete(self):
        case_data = request.json
        case_id = case_data.get("id")
        logger.info(f"接受到的id：{case_id}")
        exists = TestCase.query.filter_by(id=case_id).first()

        if exists:
            TestCase.query.filter_by(id=case_id).delete()
            db.session.commit()
            return {"code": 0, "msg": f"case id {case_id} sucess delete"}

        else:
            return {"code": 40002, "msg": f"case id {case_id} not exists"}


@login_ns.route("")
class LoginServer(Resource):

    post_parser = api.parser()
    post_parser.add_argument("username", type=str, required=True, location="json")
    post_parser.add_argument("password", type=str, required=True, location="json")

    @login_ns.expect(post_parser)
    def post(self):
        # 拿到请求信息
        user_info = request.json
        logger.debug(f"请求数据为：{user_info}")
        # 获取用户名
        username = user_info.get("username")
        # 获取密码
        password = user_info.get("password")
        logger.info(f"获取到的用户名为{username},密码为：{password}")
        # 用户名为空
        if not username:
            return {"code": 40011, "msg": "username is none"}
        # 密码为空
        if not password:
            return {"code": 40012, "msg": "password is none"}
        # 通过用户名和密码生成 user 对象
        user = UserDo(username=username, password=password)
        logger.debug(f"生成的 user 对象为：{user}")
        try:
            #  通过用户名查询用户是否存在
            user_result = db_session.query(UserDo).filter_by(username=user.username).first()
            logger.debug(f"通过用户名：{user.username} 查询的结果为 {user_result}")
            #     如果用户名不存在
            if not user_result:
                return {"code": 40013, "msg": "user is not register"}
            #     用户存在，密码错误
            if not user_result.check_password(password):
                return {"code": 40014, "msg": "password is wrong"}
            #     如果用户存在 生成token
            token = create_access_token(identity=user_result.as_dict())
            logger.debug(f"生成的 token {token}")
            # token 生成成功
            if token:
                return {"code": 0, "msg": "success", "token": token}
            # token 生成失败
            else:
                return {"code": 40021, "msg": "login error"}
        except Exception as e:
            logger.debug((e))
            return {"code": 40021, "msg": "login error"}


api.add_namespace(case_ns,"/testcase")
api.add_namespace(login_ns,"/login")

if __name__ == '__main__':
    # # 创建数据库表
    # db.create_all()
    # # 准备用户数据
    # db_session.add(UserDo(username="lily", password="123456"))
    # db_session.add(TestCase(id=1, case_title="case1", remark="note1"))
    # db_session.add(TestCase(id=2, case_title="case2", remark="note2"))
    # db_session.commit()
    app.run(debug=True)