import requests
import pytest
from back_live1.utils.log_utils import logger


@pytest.mark.parametrize(
    ["username","password","expect"],
    (
            ("lily","123456",0),
            ("","123456",40011),
            ("lily","",40012),
            ("lily1","123456",40013),
            ("lily","1234567",40014)
    )
)
def test_login(username,password,expect):
    data = {
        "username":username,
        "password":password
    }
    r = requests.post("http://127.0.0.1:5000/login",json=data)
    print(r.text)
    assert r.json().get("code") == expect

def test_testcase():
    header = {
        "Authorization":"Bearer " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY3NTY2OTU5MCwianRpIjoiYjMzZmI3MmEtYjliOC00OGVmLWJlZDEtN2RjZDMyZmQ3MTI1IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6eyJpZCI6MSwidXNlcm5hbWUiOiJsaWx5IiwicGFzc3dvcmQiOiIxMjM0NTYifSwibmJmIjoxNjc1NjY5NTkwLCJleHAiOjE2NzU2NzA0OTB9.785Q6KdpnlmpC5aMzGcFQkf6KCJFrPfp3iLbPKYeYHs"
    }
    logger.debug(f"{header}")
    r = requests.get("http://127.0.0.1:5000/testcase",headers=header)
    print(r.text)