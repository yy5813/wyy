from faker import Faker

from web_live2.page_object.login_page import LoginPage


class TestWework:
    _BASE_URL = "https://work.weixin.qq.com/wework_admin/frame"
    fake = Faker("zh.EN")
    __USERNAME_VALUE1 = fake.name()
    __PHONE_VALUE1 = fake.phone_number()
    __ACCTID_VALUE1 = fake.ssn()
    __USERNAME_VALUE2 = fake.name()
    __PHONE_VALUE2 = fake.phone_number()
    __ACCTID_VALUE2 = fake.ssn()
    __DEPARTMENT_NAME_VALURE = fake.job()

    def setup_class(self):
        self.home = LoginPage().login()

    def teardown_class(self):
        self.home.do_quit()

    def teardown(self):
        self.home.do_get(self._BASE_URL)

    def test_add_member_pagehome(self):
        res = self.home\
            .go_to_add_member()\
            .add_member(self.__USERNAME_VALUE1,self.__ACCTID_VALUE1,self.__PHONE_VALUE1)\
            .get_add_member_result()

        assert res == "保存成功"


    def test_add_member_list(self):
        res = self.home\
            .go_to_address_list()\
            .go_to_add_member()\
            .add_member(self.__USERNAME_VALUE2,self.__ACCTID_VALUE2,self.__PHONE_VALUE2)\
            .get_add_member_result()

        assert res == "保存成功"


    def test_add_department(self):
        res = self.home\
            .go_to_address_list()\
            .go_to_add_department()\
            .add_department(self.__DEPARTMENT_NAME_VALURE)\
            .get_add_department_result()

        assert res == "新建部门成功"

