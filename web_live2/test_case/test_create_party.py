import time

import yaml
from faker import Faker
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from test_litemail.utils.log_utils import logger


class TestCreateParty:
    def setup_class(self):
        fake = Faker("zh_CN")
        self.department = fake.job()
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()
        # 1.登录
        # 1.访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2.获取本地的cookie
        with open("../data/cookie.yaml", "r", encoding="utf-8") as f:
            cookies = yaml.safe_load(f)
        # 3.植入cookie
        for cookie in cookies:
            self.driver.add_cookie(cookie)
        # 4.访问通讯录页面
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#contacts")

    def teardown_class(self):
        self.driver.quit()

    def test_create_party(self):
        logger.info(self.department)
        # 1.点击+号
        self.driver.find_element(By.CSS_SELECTOR,".member_colLeft_top_addBtn").click()
        # 2。点击添加部门
        self.driver.find_element(By.LINK_TEXT,"添加部门").click()
        # 3.输入部门名称
        self.driver.find_elements(By.CSS_SELECTOR,".qui_inputText.ww_inputText")[1].send_keys(self.department)
        # 4.选择所属部门
        self.driver.find_element(By.LINK_TEXT,"选择所属部门").click()
        self.driver.find_element(By.XPATH,"//div[@class='inputDlg_item']//a[text()='测试企业']").click()
        time.sleep(5)
        # 5.点击确定按钮
        self.driver.find_element(By.LINK_TEXT,"确定").click()
        # 6.断言
        WebDriverWait(self.driver, 10).until(expected_conditions.visibility_of_element_located(
            (By.ID, "js_tips")
        ))
        ele = self.driver.find_element(By.ID, "js_tips").text
        assert ele == '新建部门成功'