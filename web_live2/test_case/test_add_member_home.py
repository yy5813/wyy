import yaml
from faker import Faker
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from test_litemail.utils.log_utils import logger


class TestAddMemberFromHome:
    def setup_class(self):
        fake = Faker("zh_CN")
        self.username = fake.name()
        self.acctid = fake.ssn()
        self.mobile = fake.phone_number()

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()
        # 1.登录
        # 1.访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2.获取本地的cookie
        with open("../data/cookie.yaml", "r", encoding="utf-8") as f:
            cookies = yaml.safe_load(f)
        # 3.植入cookie
        for cookie in cookies:
            self.driver.add_cookie(cookie)
        # 4.访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
    def teardown_class(self):
        self.driver.quit()


    def test_add_member(self):
        logger.info(self.username)
        # 2.点击添加成员按钮
        self.driver.find_element(By.XPATH,"//*[text()='添加成员']").click()
        # 3.填写成员信息
        # 3.1输入用户名
        self.driver.find_element(By.ID, "username").send_keys(self.username)
        # 3.2输入acctid
        self.driver.find_element(By.ID, "memberAdd_acctid").send_keys(self.acctid)
        # 3.3输入手机号
        self.driver.find_element(By.ID, "memberAdd_phone").send_keys(self.mobile)
        # 3.4点击保存
        self.driver.find_elements(By.CSS_SELECTOR, ".qui_btn.ww_btn.js_btn_save")[0].click()
        # 4.断言结果
        WebDriverWait(self.driver,10).until(expected_conditions.visibility_of_element_located(
            (By.ID,"js_tips")
        ))
        ele = self.driver.find_element(By.ID,"js_tips").text
        assert ele == '保存成功'