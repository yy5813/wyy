import time

import yaml
from selenium import webdriver


class TestWeworkLogin:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()


    def teardown_class(self):
        self.driver.quit()

    def test_save_cookies(self):
        # 1.访问登录页面
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2.手工等待
        time.sleep(20)
        # 3.获取浏览器cookies
        cookies = self.driver.get_cookies()
        # 4.保存cookies
        with open("../data/cookie.yaml", "w") as f:
            yaml.safe_dump(cookies, f)

    def test_get_cookies(self):
        # 1.访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2.获取本地的cookie
        with open("../data/cookie.yaml","r",encoding="utf-8") as f:
            cookies = yaml.safe_load(f)
        # 3.植入cookie
        for cookie in cookies:
            self.driver.add_cookie(cookie)
        # 4.访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

