import time

from faker import Faker
from selenium.webdriver.common.by import By

from web_live.utils.log_utils import logger
from web_live2.page_object.base_page import BasePage


class AddMember(BasePage):

    __USERNAME = (By.ID,"username")
    __ACCTID = (By.ID,"memberAdd_acctid")
    __PHONE = (By.ID,"memberAdd_phone")
    __BTN_ADDMEMBER = (By.LINK_TEXT,"保存")


    def add_member(self,username,acctid,phone):
        logger.info("添加成员")
        self.do_sendkeys(username,self.__USERNAME)
        self.do_sendkeys(acctid,self.__ACCTID)
        self.do_sendkeys(phone,self.__PHONE)
        self.do_click(self.__BTN_ADDMEMBER)

        from web_live2.page_object.address_list import AddressList
        return AddressList(self.driver)