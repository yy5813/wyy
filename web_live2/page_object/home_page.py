from selenium.webdriver.common.by import By


from web_live.utils.log_utils import logger
from web_live2.page_object.base_page import BasePage


class HomePage(BasePage):

    __ADD_MEMBER = (By.LINK_TEXT,"添加成员")
    __ADDRESS = (By.LINK_TEXT,"通讯录")

    def go_to_add_member(self):
        logger.info("首页点击添加成员")
        self.do_click(self.__ADD_MEMBER)

        from web_live2.page_object.add_member import AddMember
        return AddMember(self.driver)


    def go_to_address_list(self):
        logger.info("首页点击通讯录")
        self.do_click(self.__ADDRESS)

        from web_live2.page_object.address_list import AddressList
        return AddressList(self.driver)