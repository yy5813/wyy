import time

from faker import Faker
from selenium.webdriver.common.by import By


from web_live.utils.log_utils import logger
from web_live2.page_object.base_page import BasePage


class AddressList(BasePage):

    __ADD_MEMBER_RESULT = (By.ID,"js_tips")
    __ADD_MEMBER = (By.LINK_TEXT,"添加成员")
    __ADD = (By.CSS_SELECTOR,".member_colLeft_top_addBtn")
    __ADD_DEPARTMENT = (By.LINK_TEXT,"添加部门")
    __DEPARTMENT_NAME = (By.XPATH,"//*[@name='name']")
    __SELECT_DEPARTMENT = (By.CSS_SELECTOR,".js_parent_party_name")
    __DEPARTMENT = (By.XPATH,"//div[@class='inputDlg_item']//*[text()='测试企业']")
    __BTN_ADD_DEPARTMENT = (By.LINK_TEXT,"确定")


    def go_to_add_member(self):
        self.do_refresh()
        logger.info("通讯录页面，点击添加成员")
        # self.wait_element_until_clickable(self.__ADD_MEMBER)
        time.sleep(5)

        self.wait_element_until_click_exception(self.__ADD_MEMBER)

        from web_live2.page_object.add_member import AddMember
        return AddMember(self.driver)

    def get_add_member_result(self):
        logger.info("获取添加成员气泡信息")
        ele = self.wait_element_until_visibile(self.__ADD_MEMBER_RESULT)
        msg = ele.text
        logger.info(f"气泡消息：{msg}")

        return msg

    def go_to_add_department(self):
        logger.info("通讯录页面，点击添加部门")
        self.do_click(self.__ADD)
        self.do_click(self.__ADD_DEPARTMENT)

        return AddressList(self.driver)

    def add_department(self,department_name):
        logger.info("添加部门")
        self.do_sendkeys(department_name,self.__DEPARTMENT_NAME)
        self.do_click(self.__SELECT_DEPARTMENT)
        self.do_click(self.__DEPARTMENT)
        self.do_click(self.__BTN_ADD_DEPARTMENT)

        return AddressList(self.driver)

    def get_add_department_result(self):
        logger.info("获取添加部门气泡信息")
        ele = self.wait_element_until_visibile(self.__ADD_MEMBER_RESULT)
        msg = ele.text
        logger.info(f"气泡信息：{msg}")

        return msg
