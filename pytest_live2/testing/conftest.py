import time
from typing import List

import pytest
import logging

from pytest_live2.pythoncode.calculator import Calculator


def pytest_collection_modifyitems(
    session: "Session", config: "Config", items: List["Item"]
) -> None:
    for item in items:
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')


@pytest.fixture(scope="class")
def get_calc():
    calc = Calculator()

    yield calc
    print("测试结束")

@pytest.fixture(scope="function",autouse=True)
def calc_s():
    print("开始计算")
    yield
    logging.info("这是一条日志")
    print("结束计算")




@pytest.fixture(scope="session", autouse=True)
def manage_logs(request):
    """Set log file name same as test name"""
    now = time.strftime("%Y-%m-%d_%H_%M_%S")
    log_name = './log/' + now + '.logs'

    request.config.pluginmanager.get_plugin("logging-plugin") \
        .set_log_path((log_name))

