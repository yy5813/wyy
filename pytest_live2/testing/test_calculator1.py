# -*- coding:utf-8 -*-

'''
使用 fixture 提供 calc 对象
使用 fixture 实现：用例执行之前打印【开始计算】，之后【结束计算】
当前模块所有用例执行完成之后，打印【测试结束】
每条用例添加测试日志，并将日志打印输出到 logs/ <日期_时间>.log 文件中
'''

import pytest
import yaml
import allure

def get_datas(level):
    with open("../data/data.yaml") as f:
        result = yaml.safe_load(f)
        add_datas = result.get("add").get(level).get("datas")
        add_ids = result.get("add").get(level).get("ids")
    return [add_datas,add_ids]


@allure.feature("加法计算器用例")
class TestCalculator:

    add_P0_datas = get_datas("P0")[0]
    add_P0_ids = get_datas("P0")[1]
    add_P1_1_datas = get_datas("P1_1")[0]
    add_P1_1_ids = get_datas("P1_1")[1]
    add_P1_2_datas = get_datas("P1_2")[0]
    add_P1_2_ids = get_datas("P1_2")[1]
    add_P2_datas = get_datas("P2")[0]
    add_P2_ids = get_datas("P2")[1]


    @allure.story("正向用例")
    @pytest.mark.run(order=3)
    @pytest.mark.P0
    @pytest.mark.parametrize('a,b,c',add_P0_datas,ids=add_P0_ids)
    def test_case1(self,a,b,c,get_calc):
        result = get_calc.add(a,b)
        assert result == c

    @allure.story("边界值用例")
    @pytest.mark.run(order=1)
    @pytest.mark.P1_1
    @pytest.mark.parametrize('a,b,c',add_P1_1_datas,ids=add_P1_1_ids,)
    def test_case2(self,a,b,c,get_calc):
        result = get_calc.add(a,b)
        assert result == c

    @allure.story("特殊类型用例")
    @pytest.mark.run(order=2)
    @pytest.mark.P1_2
    @pytest.mark.parametrize('a,b,errortype',add_P1_2_datas,ids=add_P1_2_ids)
    def test_case3(self,a,b,errortype,get_calc):
        with pytest.raises(eval(errortype)) as e:
            result = get_calc.add(a,b)

    @allure.story("异常用例")
    @pytest.mark.run(order=4)
    @pytest.mark.P2
    @pytest.mark.parametrize('a,b,errortype',add_P2_datas,ids=add_P2_ids)
    def test_case4(self,a,b,errortype,get_calc):
        with pytest.raises(eval(errortype)) as e:
            result = get_calc.add(a,b)