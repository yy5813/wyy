# -*- coding:utf-8 -*-

import pytest
import yaml
import allure
import logging

def get_datas(level):
    with open("../data/data.yaml",encoding='utf-8') as f:
        result = yaml.safe_load(f)
        add_datas = result.get("add").get(level).get("datas")
        add_ids = result.get("add").get(level).get("ids")
        div_datas = result.get("div").get(level).get("datas")
        div_ids = result.get("div").get(level).get("ids")

    return [add_datas,add_ids,div_datas,div_ids]


@allure.feature("加法计算器用例")
class TestAddCalculator:

    add_P0_datas = get_datas("P0_1")[0]
    add_P0_ids = get_datas("P0_1")[1]
    add_P1_1_datas = get_datas("P1_1")[0]
    add_P1_1_ids = get_datas("P1_1")[1]
    add_P1_2_datas = get_datas("P1_2")[0]
    add_P1_2_ids = get_datas("P1_2")[1]
    add_P2_datas = get_datas("P2")[0]
    add_P2_ids = get_datas("P2")[1]


    @allure.story("正向用例")
    @pytest.mark.run(order=3)
    @pytest.mark.P0
    @pytest.mark.parametrize('a,b,c',add_P0_datas,ids=add_P0_ids)
    def test_case1(self,a,b,c,get_calc):

        with allure.step("相加操作"):
            result = get_calc.add(a,b)
            logging.info("预期结果：%s，实际结果：%s",c,result)
        with allure.step("结果校验"):
            assert result == c

    @allure.story("边界值用例")
    @pytest.mark.run(order=1)
    @pytest.mark.P1_1
    @pytest.mark.parametrize('a,b,c',add_P1_1_datas,ids=add_P1_1_ids,)
    def test_case2(self,a,b,c,get_calc):

        with allure.step("相加操作"):
            result = get_calc.add(a,b)
            logging.info("预期结果：%s，实际结果：%s", c, result)
        with allure.step("结果校验"):
            assert result == c

    @allure.story("特殊类型用例")
    @pytest.mark.run(order=2)
    @pytest.mark.P1_2
    @pytest.mark.parametrize('a,b,errortype',add_P1_2_datas,ids=add_P1_2_ids)
    def test_case3(self,a,b,errortype,get_calc):

        with allure.step("结果校验"):
            with pytest.raises(eval(errortype)) as e:
                result = get_calc.add(a,b)

    @allure.story("异常用例")
    @pytest.mark.run(order=4)
    @pytest.mark.P2
    @pytest.mark.parametrize('a,b,errortype',add_P2_datas,ids=add_P2_ids)
    def test_case4(self,a,b,errortype,get_calc):

        with allure.step("结果校验"):
            with pytest.raises(eval(errortype)) as e:
                result = get_calc.add(a,b)


@allure.feature("除法计算器用例")
class TestDivCalculator:

    div_P0_1_datas = get_datas("P0_1")[2]
    div_P0_1_ids = get_datas("P0_1")[3]
    div_P0_2_datas = get_datas("P0_2")[2]
    div_P0_2_ids = get_datas("P0_2")[3]
    div_P1_1_datas = get_datas("P1_1")[2]
    div_P1_1_ids = get_datas("P1_1")[3]
    div_P1_2_datas = get_datas("P1_2")[2]
    div_P1_2_ids = get_datas("P1_2")[3]
    div_P2_datas = get_datas("P2")[2]
    div_P2_ids = get_datas("P2")[3]

    @allure.story("正向用例")
    @pytest.mark.P0_1
    @pytest.mark.parametrize('a,b,c',div_P0_1_datas,ids=div_P0_1_ids)
    def test_case1(self,a,b,c,get_calc):

        with allure.step("相除操作"):
            result = get_calc.div(a,b)
            logging.info("预期结果：%s，实际结果：%s", c, result)
        with allure.step("结果校验"):
            assert result == c

    @allure.story("除数为0")
    @pytest.mark.P0_2
    @pytest.mark.parametrize('a,b,errorType',div_P0_2_datas,ids=div_P0_2_ids)
    def test_case2(self,get_calc,a,b,errorType):
        with allure.step("结果校验"):
            with pytest.raises(eval(errorType)) as e:
                result = get_calc.div(77, 0)

    @allure.story("边界值用例")
    @pytest.mark.P1_1
    @pytest.mark.parametrize('a,b,c',div_P1_1_datas,ids=div_P1_1_ids)
    def test_case3(self,get_calc,a,b,c):

        with allure.step("相除计算"):
            result = get_calc.div(a,b)
            logging.info("预期结果：%s，实际结果：%s", c, result)
        with allure.step("结果校验"):
            assert result == c

    @allure.story("特殊类型用例")
    @pytest.mark.P1_2
    @pytest.mark.parametrize('a,b,errorType',div_P1_2_datas,ids=div_P1_2_ids)
    def test_case4(self,get_calc,a,b,errorType):

        with allure.step("结果校验"):
            with pytest.raises(eval(errorType)) as e :
                result = get_calc.div(a,b)

    @allure.story("异常用例")
    @pytest.mark.P2
    @pytest.mark.parametrize('a,b,errorType',div_P2_datas,ids=div_P2_ids)
    def test_case5(self,get_calc,a,b,errorType):
        with allure.step("结果校验"):
            with pytest.raises(eval(errorType)) as e:
                result = get_calc.div(a,b)





