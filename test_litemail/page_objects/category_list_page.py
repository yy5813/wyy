from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from test_litemail.page_objects.base_page import BasePage
from test_litemail.utils.log_utils import logger


class CategoryListPage(BasePage):
    __BTN_ADD = (By.XPATH, "//*[text()='添加']")
    __MSG_ADD_OPERATE = (By.XPATH,"//*[text()='创建成功']")
    __MSG_DELETE_OPERATE = (By.XPATH, "//*[text()='删除成功']")

    def click_add(self):
        logger.info("类目列表页面，点击添加")
        """类目列表页面：点击添加"""
        # 点击“添加”按钮
        self.do_click(self.__BTN_ADD)
        # ==》创建类目页面
        from test_litemail.page_objects.category_create_page import CategoryCreatePage
        return CategoryCreatePage(self.driver)


    def get_operate_result(self):
        logger.info("获取添加操作成功")
        """类目列表页面：获取操作结果"""
        # 获取冒泡消息文本
        element = self.wait_element_until_visibile(self.__MSG_ADD_OPERATE)
        msg = element.text
        logger.info(f"冒泡消息是：{msg}")

        # ==》返回消息文本
        return msg

    def delete_category(self,category_name):
        logger.info("删除操作")
        # 对指定类目进行删除
        self.do_click((By.XPATH,f"//*[text()='{category_name}']/../..//*[text()='删除']"))
        #==》跳转到当前页面
        return CategoryListPage(self.driver)

    def get_delete_result(self):
        logger.info("类目列表页面：获取操作结果")
        """类目列表页面：获取操作结果"""
        # 获取冒泡消息文本
        element = self.wait_element_until_visibile(self.__MSG_DELETE_OPERATE)
        msg = element.text
        logger.info(f"冒泡消息是：{msg}")

        # ==》返回消息文本
        return msg
