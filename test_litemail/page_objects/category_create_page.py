import logging

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from test_litemail.page_objects.base_page import BasePage
from test_litemail.utils.log_utils import logger
from test_litemail.utils.web_util import click_exception


class CategoryCreatePage(BasePage):

    __INPUT_GATEGORY_NAME = (By.CSS_SELECTOR, ".el-input__inner")
    __BTN_CONFIRM = (By.CSS_SELECTOR, ".dialog-footer .el-button--primary")


    def create_category(self, category_name):
        logger.info("创建类目页面：创建类目")
        """创建类目页面：创建类目"""
        # 输入“类目名称”
        self.do_sendkeys(category_name,self.__INPUT_GATEGORY_NAME)
        # 点击“确定”按钮
        WebDriverWait(self.driver, 10).until(
            click_exception(*self.__BTN_CONFIRM))
        # ==》类目列表页面
        from test_litemail.page_objects.category_list_page import CategoryListPage
        return CategoryListPage(self.driver)