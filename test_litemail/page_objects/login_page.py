from selenium.webdriver.common.by import By

from test_litemail.page_objects.base_page import BasePage
from test_litemail.utils.log_utils import logger


class LoginPage(BasePage):

    _BASE_URL = "http://litemall.hogwarts.ceshiren.com"
    __INPUT_USERNAME = (By.NAME, "username")
    __INPUT_PASSWORD = (By.NAME, "password")
    __BTN_LOGIN = (By.CSS_SELECTOR, ".el-button--primary")

    def login(self):
        logger.info("登录页面，用户登录")

        """登录页面：用户登录"""
        # 访问登录页
        # 输入“用户名”
        self.do_sendkeys("manage",self.__INPUT_USERNAME)
        # 输入“密码”
        self.do_sendkeys("manage123",self.__INPUT_PASSWORD)
        # 点击“登录”按钮
        # self.do_find(self.__BTN_LOGIN).click()
        self.do_click(self.__BTN_LOGIN)


        # ==》首页
        from test_litemail.page_objects.home_page import HomePage
        return HomePage(self.driver)

