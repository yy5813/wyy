

from test_litemail.utils.log_utils import logger


def click_exception(by, element, max_attempts=5):
    def _inner(driver):
        actul_attempts = 0
        while actul_attempts < max_attempts:
            actul_attempts += 1
            try:
                driver.find_element(by, element).click()
                return True
            except Exception:
                logger.debug("点击的时候出现一次异常")
        raise Exception("超出最大点击次数")

    return _inner