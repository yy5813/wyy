import logging

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestLitemail:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)
        self.driver.maximize_window()
        # 打开页面
        self.driver.get("http://litemall.hogwarts.ceshiren.com")
        # 输入用户名、密码，问题：输入框内有值，需先清空
        username = self.driver.find_element(By.NAME, "username")
        password = self.driver.find_element(By.NAME, "password")
        username.clear()
        username.send_keys("manage")
        password.clear()
        password.send_keys("manage123")
        # 点击登录按钮
        self.driver.find_element(By.CSS_SELECTOR, ".el-button--primary").click()

        # 点击商场管理、商品类目，进入商品类目页面
        self.driver.find_element(By.XPATH, "//*[text()='商场管理']").click()
        self.driver.find_element(By.XPATH, "//*[text()='商品类目']").click()

    def teardown_class(self):
        self.driver.quit()

    # 新增功能
    def test_add_type(self):

         # 添加商品类目
         self.driver.find_element(By.XPATH, "//*[text()='添加']").click()
         self.driver.find_element(By.CSS_SELECTOR,".el-input__inner").send_keys("新增商品测试")
         #强制等待替换成显示等待
         # ele = WebDriverWait(self.driver,10).until(
         #     expected_conditions.element_to_be_clickable(
         #         (By.CSS_SELECTOR,".dialog-footer .el-button--primary")))
         # ele.click()

         #自定义显示等待
         def click_exception(by,element,max_attempts=5):
             def _inner(driver):
                 actul_attempts = 0
                 while actul_attempts<max_attempts:
                     actul_attempts += 1
                     try:
                         driver.find_element(by, element).click()
                         return True
                     except Exception:
                         logging.debug("点击的时候出现一次异常")
                 raise Exception("超出最大点击次数")
             return _inner

         WebDriverWait(self.driver,10).until(
             click_exception(
                 By.CSS_SELECTOR,".dialog-footer .el-button--primary"))

         #self.driver.find_element(By.CSS_SELECTOR,".dialog-footer .el-button--primary").click()
         # find_elements没找到会返回空列表，find_element没找到会报错
         res = self.driver.find_elements(By.XPATH,"//*[text()='新增商品测试']")
         logging.info(f"获取到的实际结果为{res}")
         assert res != []
         # 问题：产生脏数据，解决：清理脏数据
         self.driver.find_element(By.XPATH, "//*[text()='新增商品测试']/../..//*[text()='删除']").click()


    #删除功能
    def test_delete_type(self):

        # 添加商品类目
        self.driver.find_element(By.XPATH, "//*[text()='添加']").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-input__inner").send_keys("删除商品测试")
        # self.driver.find_element(By.CSS_SELECTOR, ".dialog-footer .el-button--primary").click()
        # 强制等待替换成显示等待
        ele = WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, ".dialog-footer .el-button--primary")))
        ele.click()

        #点击删除按钮
        self.driver.find_element(By.XPATH,"//*[text()='删除商品测试']/../..//*[text()='删除']").click()
        WebDriverWait(self.driver,10).until_not(
            expected_conditions.visibility_of_any_elements_located(
                (By.XPATH, "//*[text()='删除商品测试']")
            )
        )
        res = self.driver.find_elements(By.XPATH, "//*[text()='删除商品测试']")
        logging.info(f"获取到的实际结果为{res}")
        assert res == []




