import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class TestLitemail:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)
        self.driver.maximize_window()

    def teardown_class(self):
        self.driver.quit()

    # 登录功能
    def test_login(self):
        # 打开页面
        self.driver.get("http://litemall.hogwarts.ceshiren.com")
        # 输入用户名、密码，问题：输入框内有值，需先清空
        username = self.driver.find_element(By.NAME,"username")
        password = self.driver.find_element(By.NAME, "password")
        username.clear()
        username.send_keys("manage")
        password.clear()
        password.send_keys("manage123")
        # 点击登录按钮
        self.driver.find_element(By.CSS_SELECTOR,".el-button--primary").click()

    # 新增功能
    def test_add_type(self):
         # 登录
         # 打开页面
         self.driver.get("http://litemall.hogwarts.ceshiren.com")
         # 输入用户名、密码，问题：输入框内有值，需先清空
         username = self.driver.find_element(By.NAME, "username")
         password = self.driver.find_element(By.NAME, "password")
         username.clear()
         username.send_keys("manage")
         password.clear()
         password.send_keys("manage123")
         # 点击登录按钮
         self.driver.find_element(By.CSS_SELECTOR, ".el-button--primary").click()

         #点击商场管理、商品类目，进入商品类目页面
         self.driver.find_element(By.XPATH,"//*[text()='商场管理']").click()
         self.driver.find_element(By.XPATH, "//*[text()='商品类目']").click()
         # 添加商品类目
         self.driver.find_element(By.XPATH, "//*[text()='添加']").click()
         self.driver.find_element(By.CSS_SELECTOR,".el-input__inner").send_keys("新增商品测试")
         self.driver.find_element(By.CSS_SELECTOR,".dialog-footer .el-button--primary").click()
         # find_elements没找到会返回空列表，find_element没找到会报错
         res = self.driver.find_elements(By.XPATH,"//*[text()='新增商品测试']")
         assert res != []

    def test_delete_type(self):
        # 登录
        # 打开页面
        self.driver.get("http://litemall.hogwarts.ceshiren.com")
        # 输入用户名、密码，问题：输入框内有值，需先清空
        username = self.driver.find_element(By.NAME, "username")
        password = self.driver.find_element(By.NAME, "password")
        username.clear()
        username.send_keys("manage")
        password.clear()
        password.send_keys("manage123")
        # 点击登录按钮
        self.driver.find_element(By.CSS_SELECTOR, ".el-button--primary").click()

        # 点击商场管理、商品类目，进入商品类目页面
        self.driver.find_element(By.XPATH, "//*[text()='商场管理']").click()
        self.driver.find_element(By.XPATH, "//*[text()='商品类目']").click()
        # 添加商品类目
        self.driver.find_element(By.XPATH, "//*[text()='添加']").click()
        self.driver.find_element(By.CSS_SELECTOR, ".el-input__inner").send_keys("删除商品测试")
        self.driver.find_element(By.CSS_SELECTOR, ".dialog-footer .el-button--primary").click()


        #点击删除按钮
        self.driver.find_element(By.XPATH,"//*[text()='删除商品测试']/../..//*[text()='删除']").click()
        time.sleep(2)
        res = self.driver.find_elements(By.XPATH, "//*[text()='删除商品测试']")
        assert res == []




