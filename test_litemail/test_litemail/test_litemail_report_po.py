import logging
import time

import allure
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from test_litemail.page_objects.login_page import LoginPage


class TestLitemail:

    def setup_class(self):
        self.home = LoginPage().login()
    def teardown_class(self):
        self.home.do_quit()

    # 新增功能
    @pytest.mark.parametrize("category_name",["a","b","c"])
    def test_add_type(self,category_name):

        list_page = self.home\
            .go_to_category()\
            .click_add()\
            .create_category(category_name)
        res = list_page.get_operate_result()

        assert"创建成功" == res
        list_page.delete_category(category_name)


    #删除功能
    @pytest.mark.parametrize("category_name",["delA","delB","delC"])
    def test_delete_type(self,category_name):

        res = self.home\
            .go_to_category()\
            .click_add()\
            .create_category(category_name)\
            .delete_category(category_name)\
            .get_delete_result()

        assert "删除成功" == res






